package edu.rice.yanxin.main;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * This is the database for text indexing and searching.
 */
abstract public class TextDB {

    /**
     * This stores the location of a word
     */
    static public class WordLocation {
        int row, col;
        String filename;
        public WordLocation(String f, int r, int c) {
            this.filename = f;
            this.row = r;
            this.col = c;
        }

        @Override
        public String toString() {
            return "filename:  " + this.filename + "\n" +
                   "line    :  " + this.row + "\n" +
                   "col     :  " + this.col + "\n";
        }

        @Override
        public boolean equals(Object o) {
            if(!(o instanceof WordLocation)) {
                return false;
            }

            WordLocation ow = (WordLocation) o;
            return ow.row == this.row && ow.col == this.col && ow.filename.equals(this.filename);
        }

        @Override
        public int hashCode() {
            return this.filename.hashCode() + new Integer(this.row).hashCode() + new Integer(this.col).hashCode();
        }
    }

    /**
     * Index a directory which contains multiple text files
     */
    static public TextDB getIndex(String dirname) throws IOException, Dataset.DatasetException {
        return TextDiskDB.getIndex(dirname);
    }

    /**
     * Search a word and return all its indexes
     */
    public abstract List<WordLocation> search(String in_word) throws IOException, Dataset.DatasetException;

    public abstract String toString();
}
