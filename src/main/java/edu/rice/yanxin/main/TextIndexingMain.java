package edu.rice.yanxin.main;

import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;
import org.pmw.tinylog.writers.FileWriter;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class TextIndexingMain {

    static public void main(String[] args) throws IOException, Dataset.DatasetException {
        Configurator.defaultConfig()
            .writer(new FileWriter("log.txt"))
            .level(Level.ERROR)
            .activate();

        // check for command line arguments
        if(args.length < 1) {
            System.err.println("Must provide a directory for indexing.");
            System.exit(1);
        }
        String dirname = args[0];


        // index the text files in the directory
        TextDB db;
        try {
            db = TextDB.getIndex(dirname);
        } catch (IOException | Dataset.DatasetException e) {
            System.err.println("Directory does not exist. Please run the program again with a valid directory.");
            return;
        }

        // create a scanner class for user input
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter the search word using only lower case letters. Enter 'quit' to quit.");
        System.out.print("> ");
        String in_word = scanner.nextLine();
        while(!in_word.equals("quit")) {

            if(in_word.isEmpty()) {
                System.out.println("Empty word.");
            } else {
                // get all the locations of the word
                List<TextDB.WordLocation> loc_list = db.search(in_word);
                if(loc_list.isEmpty()) {
                    System.out.println("There is no index for the word " + in_word);
                } else {
                    // print them out
                    loc_list.forEach(loc -> System.out.println(loc.toString()));
                }
            }

            // read the input again
            System.out.println("Please enter the search word using only lower case letters. Enter 'quit' to quit.");
            System.out.print("> ");
            in_word = scanner.nextLine();
        }
    }
}
