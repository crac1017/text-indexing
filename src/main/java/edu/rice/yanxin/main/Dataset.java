package edu.rice.yanxin.main;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.util.Optional;

/**
 * This is a data structure stored in the disk
 */
public class Dataset {
    final private Logger logger = LoggerFactory.getLogger(TextDiskDB.class);
    public static class DatasetException extends Exception {
        String msg;
        DatasetException(String m) { this.msg = m; }
    }

    /**
     * Class for hash table entry
     */
    public static class HashEntry {
        public String word;
        public int head;
        public int tail;

        HashEntry(String w, int n, int t) {
            this.word = w;
            this.head = n;
            this.tail = t;
        }
        public String toString() {
            return "word:    " + this.word + "\n" +
                   "head:    " + this.head + "\n" +
                   "tail:    " + this.tail;
        }
    }

    /**
     * Class for index entry
     */
    public static class IndexEntry {
        public String filename;
        public int line;
        public int col;
        public int next;
        IndexEntry(String f, int l, int c, int n) {
            this.filename = f;
            this.line = l;
            this.col = c;
            this.next = n;
        }

        public String toString() {
            return "filename: " + this.filename + "\n" +
                   "line:     " + this.line + "\n" +
                   "column:   " + this.col + "\n" +
                   "next:     " + this.next;
        }
    }

    // hash entry size
    // It has a ptr to the word, a head ptr to the index list and a tail ptr
    static final public int HASH_ENTRY_SIZE = 12;

    // index entry size
    // It has a ptr to the filename, a line number, a column number and a ptr to the next index
    static final public int INDEX_SIZE = 16;


    // this stores the content of the data
    private RandomAccessFile file;

    // this stores all strings including words and filenames
    private RandomAccessFile nameFile;

    // total number of bytes in the file
    private int indexNum;
    public int getIndexNum() { return this.indexNum; }

    // total number of data entries in the first section
    private int entryNum;
    public int getEntryNum() { return this.entryNum; }

    private int stringCapacity;

    // this is the size of a string unit
    static final public int STRING_UNIT_SIZE = 4;

    // hash function for fast retrieval of words
    private int hash(String s) {
        return Hashing.murmur3_128().hashString(s, Charsets.US_ASCII).asInt();
    }

    /**
     * Create a dataset stored in the disk. The first section is the hash table. The second section stores linked lists
     * of index data.
     */
    public Dataset(String f, String name_file, int cap, int str_cap) throws IOException {
        Files.deleteIfExists(new File(f).toPath());
        Files.deleteIfExists(new File(name_file).toPath());
        this.file = new RandomAccessFile(f, "rw");
        this.nameFile = new RandomAccessFile(name_file, "rw");
        this.stringCapacity = str_cap;

        // we create cap number of hash entries first
        for(int i = 0; i < cap; ++i) {

            // this is the ptr to the word
            this.file.writeInt(-1);

            // this is the head pointer
            this.file.writeInt(-1);

            // this is the tail pointer
            this.file.writeInt(-1);
        }
        this.entryNum = cap;
        this.indexNum = 0;

        // initialize the name file
        for(int i = 0; i < this.stringCapacity; ++i) {
            for(int j = 0; j < Dataset.STRING_UNIT_SIZE; ++j) {
                this.nameFile.writeChar('\0');
            }
        }

        // put a pound sign indicate that this is the end of the file
        this.nameFile.writeChar('#');
        this.nameFile.writeChar('\0');
    }

    /**
     * Allocate a data slot on the disk and returns the slot's position
     */
    private int allocate() throws IOException {
        int start_pos = this.entryNum * Dataset.HASH_ENTRY_SIZE + this.indexNum * Dataset.INDEX_SIZE;
        this.file.seek(start_pos);

        // allocate space for new index

        // this is the ptr of the filename where the word locates
        this.file.writeInt(-1);

        // this is the line number
        this.file.writeInt(-1);

        // this is the column number
        this.file.writeInt(-1);

        // this is the link to the next index
        this.file.writeInt(-1);

        // increase the file size
        return start_pos;
    }

    /**
     * Create an entry in the first section.
     */
    private void createEntry(int pos, String word) throws IOException, DatasetException {
        if(pos > entryNum * Dataset.HASH_ENTRY_SIZE) {
            this.logger.error("Pos {} is out of bound for word '{}'. EntryNum = {}", pos, word, this.entryNum);
            throw new DatasetException("createEntry: Pos is out of bound. EntryNum = " + this.entryNum);
        }

        logger.debug("Move to position {}", pos);
        this.file.seek(pos);

        // We get the position of the word
        int word_pos = this.getStringPos(word);
        this.file.writeInt(word_pos);
        logger.debug("Word entry for word '{}' at {}: word_pos = {}", word, pos, word_pos);

        // write the head and tail
        this.file.writeInt(-1);
        this.file.writeInt(-1);
    }

    /**
     * Given a string, find its position in the name file. If it doesn't exist, create a spot in the name file for it
     * and returns the position.
     */
    private int getStringPos(String s) throws DatasetException, IOException {
        int hash_val = this.hash(s) % this.stringCapacity;
        if(hash_val < 0) {
            hash_val += this.stringCapacity;
        }
        int skip = 0;
        int hash_pos = (hash_val + skip * skip) * Dataset.STRING_UNIT_SIZE * 2;
        Optional<String> str = this.getString(hash_pos, s.length());
        while(str.isPresent() && !str.get().equals(s)) {
            logger.debug("Trying position {} from hash val {} for string '{}'", hash_pos, hash_val, s);
            skip += 1;
            hash_pos = ((hash_val + skip * skip) % this.stringCapacity) * Dataset.STRING_UNIT_SIZE * 2;
            if(hash_pos / (Dataset.STRING_UNIT_SIZE * 2) == hash_val) {
                logger.error("We cannot find a new spot for string '{}'", s);
                throw new DatasetException("createIndex: Cannot find a new spot for string " + s);
            }
            str = this.getString(hash_pos, s.length());
        }

        if(!str.isPresent()) {
            // there is no string here
            logger.info("There is no string '{}'", s);
            logger.info("Storing string '{}' at {}", s, hash_pos);

            this.nameFile.seek(hash_pos);
            this.nameFile.writeChars(s);
            this.nameFile.writeChar('\0');
        }

        logger.info("Done storing string '{}'", s);

        return hash_pos;
    }

    /**
     * Return a string at this position. If we are in the middle of a string, get the rest anyway.
     */
    private Optional<String> getString(int pos, int length) throws IOException {
        logger.debug("Getting string at position {} with length {}", pos, length);
        assert(length > 0);
        StringBuilder result = new StringBuilder();
        this.nameFile.seek(pos);

        char ch = '\0';
        int start = -1;
        for(int i = 0; i < length; ++i) {
            ch = this.nameFile.readChar();
            if(ch != '\0') {
                start = i;
                break;
            }
        }

        logger.debug("Start = {}", start);

        if(start == -1) {
            return Optional.empty();
        } else {
            while(ch != '\0') {
                result.append(ch);
                ch = this.nameFile.readChar();
            }
            String r = result.toString();
            logger.debug("Found string '{}'", r);
            return Optional.of(r);
        }
    }

    /**
     * Get the hash table entry for a given hash table position.
     */
    public Optional<HashEntry> getEntry(int pos) throws IOException {
        file.seek(pos);

        // get the word length to see whether there is an hash entry here or not
        int wordPtr = file.readInt();
        logger.debug("Word ptr = {} at position {}", wordPtr, pos);
        if(wordPtr == -1) {
            // no entry
            return Optional.empty();
        } else {
            String word = this.getString(wordPtr, Integer.MAX_VALUE).get();
            int head = file.readInt();
            int tail = file.readInt();
            return Optional.of(new HashEntry(word, head, tail));
        }
    }

    /**
     * Get the word index at the given position
     */
    public Optional<IndexEntry> getIndex(int pos) throws IOException {
        if(pos > this.entryNum * Dataset.HASH_ENTRY_SIZE + this.indexNum * Dataset.INDEX_SIZE) {
            return Optional.empty();
        }

        // we check whether there is an index or not
        file.seek(pos);
        int filenamePtr = file.readInt();
        if(filenamePtr == -1) {
            return Optional.empty();
        } else {
            // we return the index
            String filename = this.getString(filenamePtr, Integer.MAX_VALUE).get();
            int line = file.readInt();
            int col = file.readInt();
            int next = file.readInt();
            return Optional.of(new IndexEntry(filename, line, col, next));
        }
    }

    /**
     * Index a word. If we have never seen the word, we create a hash table entry for it as well.
     */
    public void createIndex(String word, String filename, int line, int col) throws IOException, DatasetException {
        // we look for its position in the hash table first
        int hash_val = this.hash(word) % this.entryNum;
        if(hash_val < 0) {
            hash_val += this.entryNum;
        }
        int skip = 0;
        int hash_pos = (hash_val + skip) * Dataset.HASH_ENTRY_SIZE;
        logger.debug("Trying position {} for word '{}'", hash_pos, word);
        Optional<HashEntry> entryWord = this.getEntry(hash_pos);
        logger.debug("Entry at position {} :\n {}", hash_pos, entryWord.toString());
        while(entryWord.isPresent() && !entryWord.get().word.equals(word)) {
            skip += 1;
            hash_pos = ((hash_val + skip) % this.entryNum) * Dataset.HASH_ENTRY_SIZE;
            if(hash_pos == hash_val) {
                logger.error("We cannot find a new entry for word '{}'", word);
                throw new DatasetException("createIndex: Cannot find a new entry for word " + word);
            }
            logger.debug("Trying position {} for word '{}'", hash_pos, word);
            entryWord = this.getEntry(hash_pos);
            logger.debug("Entry at position {} :\n {}", hash_pos, entryWord.toString());
        }

        if(!entryWord.isPresent()) {
            // there is no entry here
            logger.info("There is no entry for word '{}'", word);
            logger.info("Creating a new entry for word '{}' at {}", word, hash_pos);
            this.createEntry(hash_pos, word);
        }

        logger.info("Inserting index:\n  filename: '{}'\n  line: {}\n  col: {}", filename, line, col);
        this.addIndex(word, filename, line, col);
        logger.info("Done Inserting index");
    }

    /**
     * Add an index to a word
     * @param word - the word we want to index
     * @param filename - the filename where the word appears
     * @param line - the line number in the filename
     * @param col - the column number in the line
     */
    private void addIndex(String word, String filename, int line, int col) throws IOException, DatasetException {
        // get the last index position
        int last_pos = this.getLastIndex(word);
        logger.debug("The last index is at {}", last_pos);

        // allocate a new space for the new index and get its starting position
        int new_index_pos = this.allocate();
        logger.debug("New index is at {}", new_index_pos);

        // write the location on the new index position first
        {
            int entry_pos = this.getEntryPos(word);
            //if(last_pos >= this.entryNum * HASH_ENTRY_SIZE) {
            if(last_pos >= 0) {
                logger.debug("Last index position is in the index section.");
                this.file.seek(last_pos + INDEX_SIZE - 4);
                this.file.writeInt(new_index_pos);

                // we also update the tail
                this.file.seek(entry_pos + Dataset.HASH_ENTRY_SIZE - 4);
                this.file.writeInt(new_index_pos);
                logger.debug("Wrote the new index position to tail at {}.", entry_pos + HASH_ENTRY_SIZE - 4);
            } else {
                // it looks like this word doesn't have an index yet
                logger.debug("Last index position is in the hash table section");
                this.file.seek(entry_pos + HASH_ENTRY_SIZE - 8);
                logger.debug("Wrote the new index position for head and tail at {}.", entry_pos + HASH_ENTRY_SIZE - 8);
                this.file.writeInt(new_index_pos);
                this.file.writeInt(new_index_pos);
            }

        }

        // we jump to the new index
        this.file.seek(new_index_pos);
        logger.debug("Moved to new index position {}", new_index_pos);

        // write the filename first
        {
            int filename_pos = this.getStringPos(filename);
            this.file.writeInt(filename_pos);
            logger.debug("Wrote the filename '{}' to position {}", filename, filename_pos);
        }

        // write the line number and column number
        this.file.writeInt(line);
        this.file.writeInt(col);
        logger.debug("Wrote the line number {} and column number {}", line, col);

        // write the link to the next index
        this.file.writeInt(-1);
        logger.debug("Wrote the next link {}", -1);

        this.indexNum += 1;
        logger.debug("We have {} indexes so far", this.indexNum);
    }

    /**
     * Get the last index for the given word so that we can add new index at the end
     */
    private int getLastIndex(String word) throws IOException, DatasetException {
        int entry_pos = this.getEntryPos(word);
        logger.debug("Entry position for word '{}' is {}", word, entry_pos);
        this.file.seek(entry_pos + HASH_ENTRY_SIZE - 4);

        // get the link from the entry
        int result_pos = this.file.readInt();
        logger.debug("Tail points to {}", result_pos);

        return result_pos;
    }

    /**
     * Given a word, get its hash table entry position
     */
    public int getEntryPos(String word) throws IOException, DatasetException {
        // we look for its position in the hash table first
        int hash_val = this.hash(word) % this.entryNum;
        if(hash_val < 0) {
            hash_val += this.entryNum;
        }
        int skip = 0;
        int hash_pos = (hash_val + skip) * Dataset.HASH_ENTRY_SIZE;
        Optional<HashEntry> entryWord = this.getEntry(hash_pos);
        if(!entryWord.isPresent()) {
            logger.debug("Entry at position {} does not exists", hash_pos);
        } else {
            logger.debug("Entry at position {} :\n{}", hash_pos, entryWord.get().toString());
        }
        while(entryWord.isPresent() && !entryWord.get().word.equals(word)) {
            skip += 1;
            hash_pos = ((hash_val + skip) % this.entryNum) * Dataset.HASH_ENTRY_SIZE;
            entryWord = this.getEntry(hash_pos);
            if(!entryWord.isPresent()) {
                logger.debug("Entry at position {} does not exists", hash_pos);
            } else {
                logger.debug("Entry at position {} :\n{}", hash_pos, entryWord.get().toString());
            }
        }

        if(!entryWord.isPresent()) {
            throw new DatasetException("Word " + word + " is not in the hash table.");
        }
        return hash_pos;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(this.entryNum + " Hash Entries:\n");
        for(int i = 0; i < this.entryNum; ++i) {
            int pos = i * Dataset.HASH_ENTRY_SIZE;
            try {
                Optional<HashEntry> entry = this.getEntry(pos);
                if(entry.isPresent()) {
                    result.append("  position: " + pos + "\n");
                    result.append("  word:     " + entry.get().word + "\n");
                    result.append("  head:     " + entry.get().head + "\n");
                    result.append("  tail:     " + entry.get().tail + "\n");
                    result.append("\n");
                }
            } catch (IOException e) {
                logger.error("Failed to get entry at position {} : {}", pos, e.getMessage());
            }
        }

        result.append(this.indexNum + " Indexes:\n");
        for(int i = 0; i < this.indexNum; ++i) {
            int pos = this.entryNum * Dataset.HASH_ENTRY_SIZE + i * Dataset.INDEX_SIZE;
            try {
                Optional<IndexEntry> index = this.getIndex(pos);
                if(index.isPresent()) {
                    result.append("  position: " + pos + "\n");
                    result.append("  filename: " + index.get().filename + "\n");
                    result.append("  line:     " + index.get().line + "\n");
                    result.append("  col:      " + index.get().col + "\n");
                    result.append("  next:     " + index.get().next + "\n");
                    result.append("\n");

                }
            } catch (IOException e) {
                logger.error("Failed to get index at position {} : {}", pos, e.getMessage());
            }
        }

        return result.toString();
    }
}
