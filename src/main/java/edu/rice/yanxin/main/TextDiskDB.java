package edu.rice.yanxin.main;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.math3.primes.Primes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.Normalizer;
import java.util.*;

/**
 * This is an implementation of TextDB. It stores the dataset into the disk and provides functionality for indexing
 * and searching.
 */
public class TextDiskDB extends TextDB {

    final private Logger logger = LoggerFactory.getLogger(TextDiskDB.class);

    // the maximum number of unique words we can store in the dataset
    static final private int MAX_WORD_COUNT = 30000;

    // the maximum number of string units we can store. Every string unit is 4 chars
    static final private int MAX_STRING_UNIT = 60000;

    // the dataset it self
    private Dataset myData;

    // index filename
    static private final String INDEX_FILE_NAME = "index.textdb";

    // this file stores all words and filenames
    static private final String NAME_FILE_NAME = "names.textdb";

    /**
     * Main entry point for indexing a directory
     */
    static public TextDB getIndex(String dirname) throws IOException, Dataset.DatasetException {
        return new TextDiskDB(dirname);
    }


    /**
     * This constructor creates a text db from a directory by indexing all the text files in it.
     */
    public TextDiskDB(String dirname) throws IOException, Dataset.DatasetException {
        logger.info("Indexing directory '{}'", dirname);

        // list all files in the path
        File dir = new File(dirname);
        if(!dir.exists() || !dir.isDirectory()) {
            throw new Dataset.DatasetException("Directory does not exist.");
        }

        File[] file_list = dir.listFiles();

        // create a dataset
        System.out.println("Creating a dataset.");
        logger.info("Creating a dataset.");
        Dataset ds = new Dataset(dirname + "/" + INDEX_FILE_NAME, dirname + "/" + NAME_FILE_NAME, Primes.nextPrime(TextDiskDB.MAX_WORD_COUNT), Primes.nextPrime(TextDiskDB.MAX_STRING_UNIT));

        // we then iterate through all files
        int word_count = 0;
        for(int i = 0; i < file_list.length; ++i) {
            File f = file_list[i];
            long start = System.currentTimeMillis();
            if(f.isFile() && FilenameUtils.getExtension(f.getName()).equals("txt")) {
                System.out.println("Indexing file " + f.getName());
                logger.info("Indexing file '{}'", f.getName());
                try {

                    // current line number
                    int lineNum = -1;
                    Scanner scanner = new Scanner(f);
                    while(scanner.hasNextLine()) {
                        lineNum += 1;
                        int linePos = 0;
                        String line = scanner.nextLine();
                        logger.debug("Line {}: {}", lineNum, line);

                        // we split the line using empty space
                        String[] words = line.split(" ");
                        for(String word : words) {

                            // we calculate the position of the word in the line
                            int pos = line.indexOf(word, linePos);

                            // we normalize the word
                            logger.debug("See a word '{}' at '{}':{}:{}", word, f.getName(), lineNum, pos);
                            String normalized_word = Normalizer.normalize(word.toLowerCase(), Normalizer.Form.NFD).replaceAll("[^-a-z0-9]", "");
                            logger.debug("Normalized word '{}'", normalized_word);

                            // we then store the word into the dataset
                            if(normalized_word.length() > 1) {
                                ds.createIndex(normalized_word, f.getName(), lineNum, pos);
                                logger.debug("Indexed the word.");
                                linePos = (pos + word.length());
                                word_count += 1;
                            }
                        }
                    }
                } catch (FileNotFoundException e) {
                    logger.warn("Failed to get words from file '{}'", f.getName());
                }
                logger.debug("We have indexed {} words so far.", word_count);
                long end = System.currentTimeMillis();
                logger.info("Used {} seconds.", (end - start) / 1000.0);
                System.out.println("Finished. Used " + ((end - start) / 1000) + " seconds");
            }
        }
        this.myData = ds;
    }


    /**
     * Search a word and return all its indexes as a linked list of word positions
     */
    @Override
    public List<WordLocation> search(String in_word)  {
        List<WordLocation> result = new ArrayList<>(100);
        int entry_pos;
        try {

            // get the hash entry position for the given word
            entry_pos = this.myData.getEntryPos(in_word);
            Optional<Dataset.HashEntry> entry = this.myData.getEntry(entry_pos);
            if(!entry.isPresent()) {
                logger.error("There is no entry for word '{}'", in_word);
                return result;
            }

            // then we collect all its indexes
            int index_pos = entry.get().head;
            while(index_pos != -1) {
                Optional<Dataset.IndexEntry> index = this.myData.getIndex(index_pos);
                if(index.isPresent()) {
                    result.add(new WordLocation(index.get().filename, index.get().line, index.get().col));
                    index_pos = index.get().next;
                }
            }
        } catch (IOException | Dataset.DatasetException e) {
            logger.error("Failed to get entry position for word '{}' : {}", in_word, e.getMessage());
            return result;
        }

        return result;
    }

    @Override
    public String toString() {
        return this.myData.toString();
    }
}
