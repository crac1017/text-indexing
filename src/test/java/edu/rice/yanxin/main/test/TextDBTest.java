package edu.rice.yanxin.main.test;

import edu.rice.yanxin.main.Dataset;
import edu.rice.yanxin.main.TextDB;
import org.junit.Test;
import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;
import org.pmw.tinylog.writers.ConsoleWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.*;

public class TextDBTest {
    final private Logger logger = LoggerFactory.getLogger(TextDBTest.class);

    /**
     * Check the content of the dataset on some text
     */
    @Test
    public void testIndexingDirectory() throws IOException, Dataset.DatasetException {
        Configurator.defaultConfig()
            .writer(new ConsoleWriter())
            .level(Level.DEBUG)
            .activate();
        String textdb_filename = "src/test/resources/small_text";
        TextDB db = TextDB.getIndex(textdb_filename);
        assertNotNull(db);
        logger.info(db.toString());
    }

    /**
     * Check whether it founds all good words
     */
    @Test
    public void testCorrectness() throws IOException, Dataset.DatasetException {
        Configurator.defaultConfig()
            .writer(new ConsoleWriter())
            .level(Level.DEBUG)
            .activate();

        Set<TextDB.WordLocation> good_set = new HashSet<>();
        good_set.add(new TextDB.WordLocation("bar.txt", 1, 5));
        good_set.add(new TextDB.WordLocation("bar.txt", 3, 0));
        good_set.add(new TextDB.WordLocation("foo.txt", 0, 0));
        good_set.add(new TextDB.WordLocation("foo.txt", 17, 0));
        good_set.add(new TextDB.WordLocation("word.txt", 1, 30));
        good_set.add(new TextDB.WordLocation("word.txt", 3, 12));
        good_set.add(new TextDB.WordLocation("word.txt", 3, 35));
        good_set.add(new TextDB.WordLocation("word.txt", 4, 24));
        good_set.add(new TextDB.WordLocation("lisp.txt", 16, 72));

        String textdb_filename = "src/test/resources/small_text";
        TextDB db = TextDB.getIndex(textdb_filename);
        List<TextDB.WordLocation> good_words = db.search("good");

        assertEquals(good_set.size(), good_words.size());
        for(TextDB.WordLocation wl : good_words) {
            assertTrue(good_set.contains(wl));
        }
    }

    /**
     * Check the behavior on searching for a bad word
     */
    @Test
    public void testSearchingBadWords() throws IOException, Dataset.DatasetException {
        Configurator.defaultConfig()
            .writer(new ConsoleWriter())
            .level(Level.DEBUG)
            .activate();

        String textdb_filename = "src/test/resources/small_text";
        TextDB db = TextDB.getIndex(textdb_filename);
        List<TextDB.WordLocation> good_words = db.search("123gidsfdsjfdsa");
        assertTrue(good_words.isEmpty());
    }
}
