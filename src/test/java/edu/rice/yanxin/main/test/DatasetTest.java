package edu.rice.yanxin.main.test;

import edu.rice.yanxin.main.Dataset;
import org.junit.Test;
import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;
import org.pmw.tinylog.writers.ConsoleWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.util.Optional;

import static org.junit.Assert.*;

public class DatasetTest {
    final private Logger logger = LoggerFactory.getLogger(DatasetTest.class);

    /**
     * Test whether the constructor initializes everything correctly
     */
    @Test
    public void testConstructor() throws IOException {
        String textdb_filename = "src/test/resources/test_constructor.textdb";
        String name_file = "src/test/resources/test_constructor_names.textdb";
        int cap = 4;
        int string_cap = 10;
        Dataset ds = new Dataset(textdb_filename, name_file, cap, string_cap);
        assertNotNull(ds);
        assertEquals(ds.getEntryNum(), cap);
        assertEquals(ds.getIndexNum(), 0);

        RandomAccessFile file = new RandomAccessFile(textdb_filename, "r");
        for(int i = 0; i < cap; ++i) {
            assertEquals(-1, file.readInt());
            assertEquals(-1, file.readInt());
            assertEquals(-1, file.readInt());
        }

        RandomAccessFile nameFile = new RandomAccessFile(name_file, "r");
        for(int i = 0; i < string_cap * Dataset.STRING_UNIT_SIZE; ++i) {
            assertEquals('\0', nameFile.readChar());
        }
    }

    /**
     * Check the content of the dataset
     */
    @Test
    public void testCreateEntry() throws IOException, Dataset.DatasetException {
        Configurator.defaultConfig()
            .writer(new ConsoleWriter())
            .level(Level.DEBUG)
            .activate();

        String textdb_filename = "src/test/resources/test_entry.textdb";
        String name_file = "src/test/resources/test_entry_names.textdb";
        Files.deleteIfExists(new File(textdb_filename).toPath());
        int cap = 10;
        int string_cap = 100;
        Dataset ds = new Dataset(textdb_filename, name_file, cap, string_cap);

        ds.createIndex("foo", "foobar.txt", 1, 10);
        logger.info(ds.toString());
        ds.createIndex("bar", "foobar.txt", 1, 2);
        logger.info(ds.toString());
        ds.createIndex("bar", "foobar2.txt", 20, 30);
        logger.info(ds.toString());

        {
            int fooPos = ds.getEntryPos("foo");
            Optional<Dataset.HashEntry> fooEntry = ds.getEntry(fooPos);
            assertTrue(fooEntry.isPresent());
            assertEquals(fooEntry.get().word, "foo");

            int head = fooEntry.get().head;
            int tail = fooEntry.get().tail;
            assertNotEquals(-1, head);
            assertEquals(head, tail);

            Optional<Dataset.IndexEntry> fooIndex = ds.getIndex(head);
            assertTrue(fooIndex.isPresent());
            assertEquals("foobar.txt", fooIndex.get().filename);
            assertEquals(1, fooIndex.get().line);
            assertEquals(10, fooIndex.get().col);
            assertEquals(-1, fooIndex.get().next);
        }

        {
            int barPos = ds.getEntryPos("bar");
            Optional<Dataset.HashEntry> barEntry = ds.getEntry(barPos);
            assertTrue(barEntry.isPresent());
            assertEquals("bar", barEntry.get().word);

            int head = barEntry.get().head;
            int tail = barEntry.get().tail;
            assertNotEquals(-1, head);
            assertNotEquals(tail, head);

            Optional<Dataset.IndexEntry> barIndex = ds.getIndex(head);
            assertTrue(barIndex.isPresent());
            assertEquals("foobar.txt", barIndex.get().filename);
            assertEquals(1, barIndex.get().line);
            assertEquals(2, barIndex.get().col);
            assertNotEquals(-1, barIndex.get().next);

            head = barIndex.get().next;
            barIndex = ds.getIndex(head);
            assertTrue(barIndex.isPresent());
            assertEquals("foobar2.txt", barIndex.get().filename);
            assertEquals(20, barIndex.get().line);
            assertEquals(30, barIndex.get().col);
            assertEquals(-1, barIndex.get().next);
        }
    }

    /**
     * Keep inserting indexes to see whether it crashes
     */
    @Test
    public void testLargeDataset() throws IOException, Dataset.DatasetException {
        Configurator.defaultConfig()
            .writer(new ConsoleWriter())
            .level(Level.DEBUG)
            .activate();

        String textdb_filename = "src/test/resources/test_large_dataset.textdb";
        String name_file = "src/test/resources/test_large_dataset_names.textdb";
        Files.deleteIfExists(new File(textdb_filename).toPath());
        int cap = 10;
        int string_cap = 10000;
        Dataset ds = new Dataset(textdb_filename, name_file, cap, string_cap);

        for(int i = 0; i < 100; ++i) {
            ds.createIndex("foo", "foobar" + i % 10 + ".txt", i * 2, i * 3);
            ds.createIndex("bar", "foobar" + i % 5 + ".txt", i * 4, i * 5);
            ds.createIndex("word", "dic.txt", i, i);
            ds.createIndex("java", "PL.txt", i + 2, i + 5);
        }
    }

    /**
     * Check whether it throws exception when there are too many words
     */
    @Test(expected = Dataset.DatasetException.class)
    public void testTooManyEntires() throws IOException, Dataset.DatasetException {
        Configurator.defaultConfig()
            .writer(new ConsoleWriter())
            .level(Level.DEBUG)
            .activate();

        String textdb_filename = "src/test/resources/test_large_dataset.textdb";
        String name_file = "src/test/resources/test_large_dataset_names.textdb";
        Files.deleteIfExists(new File(textdb_filename).toPath());

        // use a small capacity
        int cap = 3;
        int string_cap = 1000;
        Dataset ds = new Dataset(textdb_filename, name_file, cap, string_cap);

        for(int i = 0; i < 100; ++i) {
            ds.createIndex("foo", "foobar" + i % 10 + ".txt", i * 2, i * 3);
            ds.createIndex("bar", "foobar" + i % 5 + ".txt", i * 4, i * 5);
            ds.createIndex("word", "dic.txt", i, i);
            ds.createIndex("java", "PL.txt", i + 2, i + 5);
        }
    }
}
