# Text Indexing
This program indexes a directory of text files. After the program
finishes indexing, a user can enter a word and the program will print
the locations of the occurrences of the given word. TextIndexingMain
contains the main function for execution. It accepts a directory which
contains one or more text files. It then creates a dataset in the
disk, indexes each word from all text files and stores them into the
dataset. After the program finishes indexing, the user can search the
dataset for words.

## Compilation and Running the Program
To compile the program, please make sure Maven is installed and
execute the following command:

`mvn package`

This command will create a fat jar at `target` directory.

After the compilation, enter the following to execute the program:

`java -cp target/text-indexing-1.0-SNAPSHOT.jar edu.rice.yanxin.main.TextIndexingMain {dir name}`

This command will invoke the main function in `TextIndexingMain` class
which will accept a directory name as the first command line argument.

After the program starts executing, it will first create a dataset and
index all the text files in the given directory. Please wait for it to
finish and it will ask the user for inputs.

There are two sample directories for users to try out. The first one
is located at `src/test/resources/small_text` which contains several
small text files, so users don't need to wait for a long time before
they can start searching for words. The command to run the program
using this directory is:

`java -cp target/text-indexing-1.0-SNAPSHOT.jar edu.rice.yanxin.main.TextIndexingMain src/test/resources/small_text`

The second directory is at `src/test/resources/books` which contains
three books where the average file size is around 450K. Users need to
wait for at least several minutes for the program to finish
indexing. The command is:

`java -cp target/text-indexing-1.0-SNAPSHOT.jar edu.rice.yanxin.main.TextIndexingMain src/test/resources/books`

After indexing is done, users can type in words and the program will
print out the locations of occurrences of the given word. Please only
use lower case letters and type `quit` to exit the program.

## Dataset and Indexing
I assume that we have a very limited amount of memory, so the dataset
is stored in the disk. The main data structure I use is a hash table
where each entry points to a list of indexes and Dataset is the class
for the implementation of this data structure. MurmurHash is used to
calculate the position of a hash entry given a word for fast
retrieval. 

Since we only need to append new indexes at the end and we are not
doing any random accessing, using a linked list to store the indexes
is sufficient. We store the head and the tail of each linked list. The
head is used for retrieving all the indexes and the tail is used to
append new index at the end of the list. I tested the performance of
the program on a Macbook Pro with a 2 GHz Intel Core i5 processor, 16
GB of memory and an SSD with 256GB of storage and it takes 40 seconds
to index three books, "Pride and Prejudice" (167K),
"Alice's Adventures in Wonderland" (717K) and "The Adventures of
Sherlock Holmes" (595K), which were downloaded from project
Gutenberg. Index retrieval is quite fast and each query can be done
without any wait time.

To save space, the program creates another name file which is used
to store strings including words and filenames. In the old version,
100 characters are allocated to every string. In the new version, a
pointer to a location in the name file is created for each word or
filename and the corresponding string is stored inside the name
file. In addition, we make sure there are no duplicate strings in the
name file. Therefore, two pointers will point to the same location in
the name file if they are the same word or the same filename.

## Project Structure, Development and Testing
TextIndexMain is the main entry point of the program. It calls the
methods from TextDB class. TextDB is the class provides word indexing
and searching. TextDiskDB is an implementation of TextDB which stores
indexes in the disk. TextDB serves as an abstraction for any possible
future functionality extension.

The development was quite smooth and sometimes it requires me to
search for some external resources such as hash functions and logging
libraries. Logging which is used heavily is mainly for
debugging. Tests on correctness and performance were also conducted
using JUnit.

## Improvement since the First Version
I solved the memory issue. In the first version, I allocate 100
characters for each string including filenames and words. In that
case, there is a limit on the string size and for small strings we
will be wasting a large amount of space. In this version, we replace
every string in the index file with a pointer to a location in another
file that I call the name file which only contains strings. When we
are indexing a string, we check the name file to see whether it
already contains the string. If not, we allocate a chunk of space for
it and return its location. If so, we just return its location. Then
we create a pointer accordingly.

The difficulty in designing the data structure is how to save time and
space. To save space, the name file is actually divided into a large
amount of *string units* where each string unit contains 4
characters. We assign as many contiguous units as needed to store
strings and use '\0' to end a string. For example, "a" and "abc"
requires one unit, whereas "abcd" and "abcde" require two contiguous
units.

Since the size of each unit is fixed, we could still use a hash
function to find the starting unit for the string we want to search
for. In addition, we use quadratic probing to skip units until we find
the right string or we find an empty space for storing new
strings.

After I implemented the feature, I observed that the program uses less
space, from 60MB to 5MB. In addition, I used profiling and found out
that logging took a large amount of runtime. After reducing the amount
of logging, the program is able to index the three books mentioned
above under 40 seconds. I have already changed the text in the
previous sections accordingly.
